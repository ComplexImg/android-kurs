package by.bsuir.kurs;

import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import by.bsuir.kurs.appData.MemoryDB;
import by.bsuir.kurs.loader.UserLoader;
import by.bsuir.kurs.loader.response.RequestResult;
import by.bsuir.kurs.loader.response.Response;
import by.bsuir.kurs.model.User;
import by.bsuir.kurs.other.Validator;

public class RegisterWithMenuActivity extends BaseActivity implements LoaderManager.LoaderCallbacks<Response>{

    Button register;
    EditText login, email, pass, pass2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_with_menu);

        setTitle("Регистрация");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);



        register = (Button) findViewById(R.id.registerBtn);
        login = (EditText) findViewById(R.id.loginText);
        email = (EditText) findViewById(R.id.emailText);
        pass = (EditText) findViewById(R.id.passText);
        pass2 = (EditText) findViewById(R.id.passText2);



        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFieldsCorrect()) {
                    Toast.makeText(RegisterWithMenuActivity.this, "ок", Toast.LENGTH_SHORT).show();

                    User newUser = new User();
                    newUser.setUsername(login.getText().toString());
                    newUser.setPassword(pass.getText().toString());
                    newUser.setEmail(email.getText().toString());

                    MemoryDB.setUser(newUser);

                    getLoaderManager().initLoader(0, Bundle.EMPTY, RegisterWithMenuActivity.this);

                } else {
                    Toast.makeText(RegisterWithMenuActivity.this, "Поля заполнены неверно!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private boolean isFieldsCorrect() {
        return (Validator.checkEngString(login.getText().toString()) &&
                Validator.checkEmail(email.getText().toString()) &&
                (pass.getText().toString().equals(pass2.getText().toString())) &&
                Validator.checkEngString(pass.getText().toString()));
    }

    @Override
    public Loader<Response> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case 0:
                return new UserLoader(this);

            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Response> loader, Response data) {
        int id = loader.getId();
        if (id == 0) {
            if (data.allRight()) {
                User user = data.getTypedAnswer();

                Intent intent = new Intent(RegisterWithMenuActivity.this, MainActivity.class);
                startActivity(intent);

            } else if(data.getRequestResult() == RequestResult.BAD_USER) {
                // неверный юзер
                Toast.makeText(RegisterWithMenuActivity.this, "Неверные данные!", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(RegisterWithMenuActivity.this, "Не получилось сходить на сервер!", Toast.LENGTH_SHORT).show();
            }

        }
        getLoaderManager().destroyLoader(id);
    }

    @Override
    public void onLoaderReset(Loader<Response> loader) {
        // Do nothing
    }
}
