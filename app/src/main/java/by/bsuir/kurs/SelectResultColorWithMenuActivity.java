package by.bsuir.kurs;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import by.bsuir.kurs.adapter.ColorProportionsAdapter;
import by.bsuir.kurs.appData.MemoryDB;
import by.bsuir.kurs.appData.SharedDB;
import by.bsuir.kurs.model.ResultColor;
import by.bsuir.kurs.retrofit.Rest;
import okhttp3.Credentials;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectResultColorWithMenuActivity extends BaseActivity {

    TextView tvId, resColTextView;
    EditText titleText;
    Button saveBtn, deleteBtn, addColorBtn;

    ColorProportionsAdapter colorProportionsAdapter;
    SharedDB db;
    ResultColor resultColor;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_result_color_with_menu);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Intent intent = getIntent();
        id = intent.getIntExtra("id", -1);

        tvId = (TextView) findViewById(R.id.tvId);
        resColTextView = (TextView) findViewById(R.id.resColTextView);

        titleText = (EditText) findViewById(R.id.titleText);
        saveBtn = (Button) findViewById(R.id.saveBtn);
        deleteBtn = (Button) findViewById(R.id.deleteBtn);
        addColorBtn = (Button) findViewById(R.id.addColorBtn);

        db = SharedDB.getObj(SelectResultColorWithMenuActivity.this);

        resultColor = MemoryDB.getLocalResultColor();

        if (resultColor.getId() == null && id == -1) {
            setTitle("Новый цвет");
            deleteBtn.setEnabled(false);
        } else {
            setTitle("Сохраненный цвет");
            deleteBtn.setEnabled(true);
        }

        if (resultColor != null) {

            resColTextView.setText("#" + resultColor.getColor());
            resColTextView.setBackgroundColor(Color.parseColor("#" + resultColor.getColor()));


            if (resultColor != null && resultColor.getProportions() != null) {
                tvId.setText("#" + (resultColor.getId() != null ? resultColor.getId() : "New"));
                titleText.setText(resultColor.getTitle());
                ListView lv = (ListView) findViewById(R.id.selectPaletteColors);
                colorProportionsAdapter = new ColorProportionsAdapter(this, resultColor.getProportions(), id);
                lv.setAdapter(colorProportionsAdapter);
            }
        }


        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultColor.setTitle(titleText.getText().toString());

                if ((resultColor.getId() != null && id != -1) || (resultColor.getId() == null && id != -1)) {
                    if (MemoryDB.isNetwork()) {
                        Rest.getResultColorApi().update(Credentials.basic(MemoryDB.getUser().getUsername(), MemoryDB.getUser().getPassword()), resultColor).enqueue(new Callback<ResultColor>() {
                            @Override
                            public void onResponse(Call<ResultColor> call, Response<ResultColor> response) {
                                Toast.makeText(SelectResultColorWithMenuActivity.this, "OK!", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onFailure(Call<ResultColor> call, Throwable t) {
                                Toast.makeText(SelectResultColorWithMenuActivity.this, "Не получилось сходить на сервер!", Toast.LENGTH_SHORT).show();
                                MemoryDB.setNetwork(false);
                                db.setColorsSyncFlag(false);
                            }
                        });
                    }
                    db.updateResultColors(MemoryDB.getColors());
                    finish();
                } else {
                    if (MemoryDB.isNetwork()) {
                        Rest.getResultColorApi().add(Credentials.basic(MemoryDB.getUser().getUsername(), MemoryDB.getUser().getPassword()), resultColor).enqueue(new Callback<ResultColor>() {
                            @Override
                            public void onResponse(Call<ResultColor> call, Response<ResultColor> response) {
                                Toast.makeText(SelectResultColorWithMenuActivity.this, "OK!", Toast.LENGTH_SHORT).show();
                                ResultColor answerColor = response.body();
                                MemoryDB.getColors().add(answerColor);
                                SharedDB db = SharedDB.getObj(SelectResultColorWithMenuActivity.this);
                                db.updateResultColors(MemoryDB.getColors());
                                startActivity(new Intent(SelectResultColorWithMenuActivity.this, ResultColorsListWithMenu.class));
                            }

                            @Override
                            public void onFailure(Call<ResultColor> call, Throwable t) {
                                Toast.makeText(SelectResultColorWithMenuActivity.this, "Не получилось сходить на сервер!", Toast.LENGTH_SHORT).show();
                                MemoryDB.setNetwork(false);
                                db.setColorsSyncFlag(false);
                                startActivity(new Intent(SelectResultColorWithMenuActivity.this, ResultColorsListWithMenu.class));
                            }
                        });
                    } else {
                        MemoryDB.getColors().add(resultColor);
                        SharedDB db = SharedDB.getObj(SelectResultColorWithMenuActivity.this);
                        db.updateResultColors(MemoryDB.getColors());
                        db.setColorsSyncFlag(false);
                        startActivity(new Intent(SelectResultColorWithMenuActivity.this, ResultColorsListWithMenu.class));
                    }
                }


            }
        });

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder ad;
                ad = new AlertDialog.Builder(SelectResultColorWithMenuActivity.this);
                ad.setTitle("Вы уверены что хотите...");
                ad.setMessage("ЭТОТ ЦВЕТ?!");
                ad.setPositiveButton("Отмена", null);
                ad.setNegativeButton("Да", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {

                        if (MemoryDB.isNetwork()) {
                            Rest.getResultColorApi().delete(Credentials.basic(MemoryDB.getUser().getUsername(), MemoryDB.getUser().getPassword()), resultColor.getId()).enqueue(new Callback<Boolean>() {
                                @Override
                                public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                                    MemoryDB.getColors().remove(id);
                                    db.updateResultColors(MemoryDB.getColors());
                                    finish();
                                }

                                @Override
                                public void onFailure(Call<Boolean> call, Throwable t) {
                                    Toast.makeText(SelectResultColorWithMenuActivity.this, "Не получилось сходить на сервер!", Toast.LENGTH_SHORT).show();
                                    MemoryDB.getColors().remove(id);
                                    MemoryDB.setNetwork(false);
                                    db.updateResultColors(MemoryDB.getColors());
                                    db.setColorsSyncFlag(false);
                                    finish();
                                }
                            });
                        } else {
                            MemoryDB.getColors().remove(id);
                            db.updateResultColors(MemoryDB.getColors());
                            db.setColorsSyncFlag(false);
                            finish();
                        }

                    }
                });
                ad.show();
            }
        });

    }
}
