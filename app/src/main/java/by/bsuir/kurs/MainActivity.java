package by.bsuir.kurs;

import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import by.bsuir.kurs.appData.MemoryDB;
import by.bsuir.kurs.appData.SharedDB;
import by.bsuir.kurs.loader.PalettesLoader;
import by.bsuir.kurs.loader.ResultColorsLoader;
import by.bsuir.kurs.loader.response.RequestResult;
import by.bsuir.kurs.loader.response.Response;
import by.bsuir.kurs.model.Palette;
import by.bsuir.kurs.model.ResultColor;
import by.bsuir.kurs.model.User;
import by.bsuir.kurs.other.Validator;
import by.bsuir.kurs.retrofit.Rest;
import okhttp3.Credentials;
import retrofit2.Call;
import retrofit2.Callback;

public class MainActivity extends BaseActivity implements LoaderManager.LoaderCallbacks<Response> {

    boolean palettesLoadFlag, resColorsLoadFlag;

    EditText login, pass;
    Button registerBtn, loginBtn;
    SharedDB db;

    boolean paletteSyncFlag = false;
    boolean colorsSyncFlag = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_with_menu);

        setTitle("Вход в аккаунт");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        login = (EditText) findViewById(R.id.loginText);
        pass = (EditText) findViewById(R.id.passText);
        registerBtn = (Button) findViewById(R.id.registerBtn);
        loginBtn = (Button) findViewById(R.id.loginBrn);


        db = SharedDB.getObj(MainActivity.this);

        String tmp = db.getServerAddress();

        MemoryDB.setServerAddress(db.getServerAddress());

        MemoryDB.setColors(db.getResultColors());
        MemoryDB.setPalettes(db.getPalettes());


        if (MemoryDB.getUser() == null) {

            MemoryDB.setUser(db.getUser());

            if (MemoryDB.getUser() != null && MemoryDB.getUser().getUsername() != null) {
                login.setText(MemoryDB.getUser().getUsername());
                pass.setText(MemoryDB.getUser().getPassword());
                syncData();
            }

        } else {
            login.setText(MemoryDB.getUser().getUsername());
            pass.setText(MemoryDB.getUser().getPassword());
        }


        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                palettesLoadFlag = false;
                resColorsLoadFlag = false;
                syncData();
            }
        });

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, RegisterWithMenuActivity.class);
                startActivity(intent);
            }
        });
    }


    private void syncData() {

        User user = new User();
        user.setUsername(login.getText().toString());
        user.setPassword(pass.getText().toString());
        MemoryDB.setUser(user);

        if (! Validator.checkEngString(user.getPassword()) ||  !Validator.checkEngString(user.getUsername())){
            Toast.makeText(MainActivity.this, "Некорректные данные. ([a-zA-Z0-9_]{3,32})", Toast.LENGTH_SHORT).show();
            return;
        }


        if (!db.getPalettesSyncFlag()) {
            Rest.getSyncApi().syncPalettes(Credentials.basic(MemoryDB.getUser().getUsername(), MemoryDB.getUser().getPassword()), db.getPalettes()).enqueue(new Callback<Boolean>() {
                @Override
                public void onResponse(Call<Boolean> call, retrofit2.Response<Boolean> response) {
                    Boolean result = response.body();
                    db.setPalettesSyncFlag(result);
                    paletteSyncFlag = true;
                    if (colorsSyncFlag) {
                        loadData();
                    }
                }

                @Override
                public void onFailure(Call<Boolean> call, Throwable t) {
                    paletteSyncFlag = true;
                    if (colorsSyncFlag) {
                        loadData();
                    }
                }
            });
        } else {
            paletteSyncFlag = true;
            if (colorsSyncFlag) {
                loadData();
            }
        }


        if (!db.getColorsSyncFlag()) {
            Rest.getSyncApi().syncColors(Credentials.basic(MemoryDB.getUser().getUsername(), MemoryDB.getUser().getPassword()), db.getResultColors()).enqueue(new Callback<Boolean>() {
                @Override
                public void onResponse(Call<Boolean> call, retrofit2.Response<Boolean> response) {
                    Boolean result = response.body();
                    db.setColorsSyncFlag(result);
                    colorsSyncFlag = true;
                    if (paletteSyncFlag) {
                        loadData();
                    }
                }

                @Override
                public void onFailure(Call<Boolean> call, Throwable t) {
                    colorsSyncFlag = true;
                    if (paletteSyncFlag) {
                        loadData();
                    }
                }
            });

        } else {
            colorsSyncFlag = true;
            if (paletteSyncFlag) {
                loadData();
            }
        }


    }

    private void loadData() {
        Toast.makeText(MainActivity.this, "Синхронизация", Toast.LENGTH_SHORT).show();

        getLoaderManager().initLoader(0, Bundle.EMPTY, MainActivity.this);
        getLoaderManager().initLoader(1, Bundle.EMPTY, MainActivity.this);
    }


    @Override
    public Loader<Response> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case 0:
                return new PalettesLoader(this);

            case 1:
                return new ResultColorsLoader(this);


            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Response> loader, Response data) {
        int id = loader.getId();
        if (id == 0) {
            if (data.allRight()) {
                List<Palette> palettes = data.getTypedAnswer();
                SharedDB db = SharedDB.getObj(MainActivity.this);

                db.updateUser(MemoryDB.getUser());

                palettesLoadFlag = true;

                if (resColorsLoadFlag) {
                    goToNextPage();
                }

            } else if (data.getRequestResult() == RequestResult.BAD_USER) {
                // неверный юзер
                Toast.makeText(MainActivity.this, "Неверные данные для входа!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(MainActivity.this, "Не получилось сходить на сервер!", Toast.LENGTH_SHORT).show();

            }

        } else if (id == 1) {
            if (data.allRight()) {
                List<ResultColor> resultColors = data.getTypedAnswer();
                SharedDB db = SharedDB.getObj(MainActivity.this);

                db.updateUser(MemoryDB.getUser());

                resColorsLoadFlag = true;

                if (palettesLoadFlag) {
                    goToNextPage();
                }


            } else if (data.getRequestResult() == RequestResult.BAD_USER) {
                // неверный юзер
                Toast.makeText(MainActivity.this, "Неверные данные для входа!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(MainActivity.this, "Не получилось сходить на сервер!", Toast.LENGTH_SHORT).show();

            }
        }
        getLoaderManager().destroyLoader(id);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (MemoryDB.getUser() == null) {

            MemoryDB.setUser(db.getUser());

            if (MemoryDB.getUser() != null && MemoryDB.getUser().getUsername() != null) {
                login.setText(MemoryDB.getUser().getUsername());
                pass.setText(MemoryDB.getUser().getPassword());
                syncData();
            }

        } else {
            login.setText(MemoryDB.getUser().getUsername());
            pass.setText(MemoryDB.getUser().getPassword());
        }
    }

    @Override
    public void onLoaderReset(Loader<Response> loader) {
        // Do nothing
    }

    public void goToNextPage() {
        Intent intent = new Intent(MainActivity.this, PalettesListWithMenuActivity.class);
        startActivity(intent);
    }

}
