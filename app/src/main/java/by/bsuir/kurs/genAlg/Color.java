package by.bsuir.kurs.genAlg;

public class Color {

    private int red, green, blue;
    private String hex;

    public Color(String color){
        this.hex = color;
        red = Integer.parseInt(color.substring(0,2), 16);
        green = Integer.parseInt(color.substring(2,4),16);
        blue = Integer.parseInt(color.substring(4,6),16);
    }

    public Color(int red, int green, int blue){
        this.red = red;
        this.green = green;
        this.blue = blue;

        hex = (Integer.toHexString(red).length() != 2 ? "0" + Integer.toHexString(red) : Integer.toHexString(red));
        hex += (Integer.toHexString(green).length() != 2 ? "0" + Integer.toHexString(green) : Integer.toHexString(green));
        hex += (Integer.toHexString(blue).length() != 2 ? "0" + Integer.toHexString(blue) : Integer.toHexString(blue));
    }

    public int getDec(){
        return Integer.parseInt(hex, 16);
    }

    public int getRed() {
        return red;
    }

    public int getGreen() {
        return green;
    }

    public int getBlue() {
        return blue;
    }

    public String getHexColor() {
        return hex;
    }

    @Override
    public String toString() {
        return "Color{" +
                "red=" + red +
                ", green=" + green +
                ", blue=" + blue +
                ", hexColor='" + hex + '\'' +
                '}';
    }
}
