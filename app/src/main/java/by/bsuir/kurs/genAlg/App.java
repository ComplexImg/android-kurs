package by.bsuir.kurs.genAlg;


import java.util.Map;

public class App {

    private Generation mainGeneration;
    private Color color;

    private int maxRepeatCount = 5, iterationCountForNextBestChromosome = 50;

    public App(Generation mainGeneration, Color color){
        this.color = color;
        this.mainGeneration = mainGeneration;

        Chromosome.setListProp(mainGeneration);
    }

    public void setSetting(int maxRepeatCount, int iterationCountForNextBestChromosome){
        this.maxRepeatCount = maxRepeatCount;
        this.iterationCountForNextBestChromosome = iterationCountForNextBestChromosome;
    }

    public Chromosome goWork(){
        Generation generation = this.mainGeneration;
        Generation newGeneration;

        Chromosome bestChromosome = new Chromosome(new Color("ffffff"));
        bestChromosome.setCompare(1000);

        int middleValue = 0, middleCounter = 0, time = 0, i = 0;

        while(true){

            ++i;
            generation.fitnessValuesSetup(color);

         //   System.out.println("Generation #" + i);
            newGeneration = generation.newGeneration(mainGeneration);
           // System.out.print(generation.getInfoForAnswer());


            float localMiddleValue = 0;
            // если обнаружен новый минимум - сохр и обновляем счетчик
            ++time;
            for (Map.Entry<Float, Chromosome> entry : generation.getBestChromosomes().entrySet()) {
                localMiddleValue += entry.getKey();
                if (entry.getValue().getCompare() < bestChromosome.getCompare()){
                    bestChromosome = entry.getValue();
                    time = 0;
                }
            }


            int newMiddleValue = (int) Math.floor(localMiddleValue / (float) generation.getBestChromosomes().size());
            if(Math.abs(newMiddleValue - middleValue) <= 1){
                middleCounter++;
            }else {
                middleValue = newMiddleValue;
                middleCounter = 0;
            }

            // если давно не было новых минимумов - выходим
            if(time > this.iterationCountForNextBestChromosome || middleCounter > maxRepeatCount){
                break;
            }







            generation = newGeneration;

        }

        System.out.println("RESULT :  " + bestChromosome.toString());

        return bestChromosome;

    }

}
