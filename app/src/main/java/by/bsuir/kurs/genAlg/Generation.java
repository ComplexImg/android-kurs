package by.bsuir.kurs.genAlg;

import java.util.HashMap;
import java.util.Map;

public class Generation {

    private int exp = 5;    // процент рандомных хромосом, которые могут размножаться
    private int bestArrayCount = 3;      // длинна массива лучших в поколении хромосом
    private int tempCount;

    private float fitnessSum;
    private Map<Float, Chromosome> bestChromosomes = new HashMap<Float, Chromosome>();
    private Map<String, Chromosome> chromosomes;



    public Generation(Map<String, Chromosome> chromosomes) {
        this.chromosomes = chromosomes;
    }

    public Map<String, Chromosome> getChromosomes() {
        return chromosomes;
    }

    public void fitnessValuesSetup(Color resultColor) {
        fitnessSum = Fitness.fitnessSum(this, resultColor);

        for (Map.Entry<String, Chromosome> entry : chromosomes.entrySet()) {
            float compare = Fitness.compareColors(entry.getValue().getColor(), resultColor);
            entry.getValue().setCompare(compare);
            entry.getValue().setFitnessValue(compare / fitnessSum);
        }
    }

    public Generation getValuesForNextGeneration() {
        float middleFitnessValue = (float) 1 / (float) chromosomes.size();

        Map<String, Chromosome> result = new HashMap<String, Chromosome>();

        for (Map.Entry<String, Chromosome> entry : chromosomes.entrySet()) {

            String key = "";
            if (entry.getValue().getFitnessValue() < middleFitnessValue && result.get(entry.getValue().getColor().getHexColor()) == null) {
                key = entry.getValue().getColor().getHexColor();
                result.put(key, entry.getValue());
            } else if (exp >= (int) (Math.random() * 100)) {
                key = entry.getValue().getColor().getHexColor();
                result.put(key, entry.getValue());
            }

            if (!key.equals("")) {
                addChromosomeInArray(entry.getValue());
            }
        }
        return new Generation(result);
    }


    private void addChromosomeInArray(Chromosome chromosome) {
        if (this.bestChromosomes.size() < this.bestArrayCount) {
            this.bestChromosomes.put(chromosome.getCompare(), chromosome);
        } else {
            float maxKey = 0;
            for (Map.Entry<Float, Chromosome> entry : bestChromosomes.entrySet()) {
                if (maxKey < entry.getValue().getCompare()) {
                    maxKey = entry.getValue().getCompare();
                }
            }

            if (chromosome.getCompare() < maxKey) {
                bestChromosomes.remove(maxKey);
                bestChromosomes.put(chromosome.getCompare(), chromosome);

            }

        }
    }


    public String getInfoForAnswer() {

        String result = "";

        result =
                "Fitness sum: " + fitnessSum +
                        "\nChromosomes count: " + chromosomes.size() +
                        "\nChromosomes for next generation: " + tempCount +
                        "\nMiddle FitnessValue: " + ((float) 1 / (float) chromosomes.size()) + "\n";
        result += "Best chromosomes:\n";

        for (Map.Entry<Float, Chromosome> entry : bestChromosomes.entrySet()) {
            Chromosome chromosome = entry.getValue();
            result += "     FV: " + chromosome.getColor().getDec() + "   compare: " + chromosome.getCompare() + "   color: " + chromosome.getColor().getHexColor() + "  \n";
        }

        return result += "\n";
    }


    public Generation newGeneration(Generation mainGeneration) {
        Map<String, Chromosome> newChromosomes = new HashMap<String, Chromosome>();

        Generation valuesForNextGeneration = getValuesForNextGeneration();
        tempCount = valuesForNextGeneration.getChromosomes().size();

        for (Chromosome mainChromosome : mainGeneration.getChromosomes().values()) {
            for (Chromosome nextGenChromosome : valuesForNextGeneration.getChromosomes().values()) {
                Chromosome newChromosome = new Chromosome(mixer(mainChromosome.getColor(), nextGenChromosome.getColor()));
                if (newChromosomes.get(newChromosome.getColor().getHexColor()) == null) {
                    newChromosome.setProp(mainChromosome.getColor(), nextGenChromosome.getProp());
                    newChromosomes.put(newChromosome.getColor().getHexColor(), newChromosome);
                }
            }
        }
        return new Generation(newChromosomes);
    }


    public Color mixer(Color color1, Color color2) {
        if (color1.equals(color2)) {
            return color1;
        } else {
            int red = (int) Math.floor((color1.getRed() + color2.getRed()) / 2);
            int green = (int) Math.floor((color1.getGreen() + color2.getGreen()) / 2);
            int blue = (int) Math.floor((color1.getBlue() + color2.getBlue()) / 2);

            return new Color(red, green, blue);
        }
    }

    public Map<Float, Chromosome> getBestChromosomes() {
        return bestChromosomes;
    }
}
