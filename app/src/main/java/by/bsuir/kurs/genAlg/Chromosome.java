package by.bsuir.kurs.genAlg;

import java.util.HashMap;
import java.util.Map;

public class Chromosome {

    private Color color;
    private float fitnessValue, compare;

    private Map<String, Integer> prop = null;
    private static Map<String, Integer> colorsListProp = null;


    public Chromosome(Color color) {
        this.color = color;
    }

    public void setProp(Color color, Map<String, Integer> oldValues) {

        if (oldValues != null) {
            prop = new HashMap<String, Integer>(oldValues);
        }else{
            prop = new HashMap<String, Integer>(colorsListProp);
        }

        Integer temp = prop.get(color.getHexColor());
        prop.remove(color.getHexColor());
        prop.put(color.getHexColor(), temp + 1);
    }

    public static void setListProp(Generation mainGeneration) {
        colorsListProp = new HashMap<String, Integer>();

        for (Map.Entry<String, Chromosome> entry : mainGeneration.getChromosomes().entrySet()) {
            colorsListProp.put(entry.getValue().getColor().getHexColor(), 0);
        }
    }

    public Map<String, Integer> getProp() {
        return prop;
    }

    public float getFitnessValue() {
        return fitnessValue;
    }

    public void setFitnessValue(float fitnessValue) {
        this.fitnessValue = fitnessValue;
    }

    public float getCompare() {
        return compare;
    }

    public void setCompare(float compare) {
        this.compare = compare;
    }

    public Color getColor() {
        return color;
    }


    @Override
    public String toString() {
        return "Chromosome{" +
                "color=" + color +
                ", fitnessValue=" + fitnessValue +
                ", compare=" + compare +
                ", prop=" + prop +
                '}';
    }
}
