package by.bsuir.kurs.genAlg;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.os.Bundle;

import by.bsuir.kurs.appData.MemoryDB;
import by.bsuir.kurs.model.Palette;
import by.bsuir.kurs.model.ResultColor;

public class ResultColorAlgLoader extends AsyncTaskLoader<ResultColor> {

    String color;
    Palette palette;

    public ResultColorAlgLoader(Context context, Bundle args) {
        super(context);

        color = args.getString("color");
        if(MemoryDB.getPalettes().size() > 0)
            palette = MemoryDB.getPalettes().get(args.getInt("paletteId"));

    }

    @Override
    public ResultColor loadInBackground() {
        try {
            ResultColor resultColor;

            Alg alg = new Alg(color, palette);

            resultColor = alg.goWork();

            return resultColor;

        } catch (Exception ex) {
            return  null;
        }

    }
}
