package by.bsuir.kurs.genAlg;

import java.util.Map;

public class Fitness {

    // евклидово расстояние в цветовом кубе 0 - Один цвет, чем меньше, тем лучше
    static float compareColors(Color color1, Color color2) {
        int red = color1.getRed() - color2.getRed();
        int blue = color1.getBlue() - color2.getBlue();
        int green = color1.getGreen() - color2.getGreen();
        return (float) Math.sqrt(red * red + blue * blue + green * green);
    }


    // сумма значений фитнесс-функции для определения среднего значения в дальнейшем
    static float fitnessSum(Generation generation, Color resultColor) {
        float result = 0;

        for (Map.Entry<String, Chromosome> entry : generation.getChromosomes().entrySet()) {
            result += compareColors(entry.getValue().getColor(), resultColor);
        }

        return result;
    }

}
