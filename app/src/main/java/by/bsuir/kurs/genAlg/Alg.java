package by.bsuir.kurs.genAlg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import by.bsuir.kurs.model.Palette;
import by.bsuir.kurs.model.Proportion;
import by.bsuir.kurs.model.ResultColor;

public class Alg {

    Palette palette;
    String color;

    public Alg(String color, Palette palette){
        this.color = color;
        this.palette = palette;
    }

    public ResultColor goWork() {

        Color resultColor = new Color(color);

        Map<String, Chromosome> mainChromosomesList = new HashMap<String, Chromosome>();

        for (String palColor: palette.getColors()){
            mainChromosomesList.put(palColor, new Chromosome(new Color(palColor)));
        }

        Generation mainGeneration = new Generation(mainChromosomesList);

        App app = new App(mainGeneration, resultColor);

        Chromosome resultChromosome = app.goWork();

        ResultColor newResult = new ResultColor();

        newResult.setColor(resultChromosome.getColor().getHexColor());

        newResult.setProportions(new ArrayList<Proportion>());

        for (Map.Entry<String, Integer> entry :resultChromosome.getProp().entrySet()) {

            if (entry.getValue() > 0){
                newResult.getProportions().add(new Proportion(entry.getKey(), entry.getValue()));
            }
        }

        return newResult;


    }
}
