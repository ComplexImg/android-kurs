package by.bsuir.kurs.adapter;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import by.bsuir.kurs.R;
import by.bsuir.kurs.SelectPaletteWithMenuActivity;
import by.bsuir.kurs.appData.MemoryDB;
import by.bsuir.kurs.model.Palette;

public class PalettesAdapter extends BaseAdapter {

    Context ctx;
    LayoutInflater lInflater;
    List<Palette> palettes;

    public PalettesAdapter(Context context, List<Palette> palettes) {
        ctx = context;
        this.palettes = palettes;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return palettes.size();
    }

    @Override
    public Object getItem(int position) {
        return palettes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.palette_item, parent, false);
        }

        Palette p = getPalette(position);


        ((TextView) view.findViewById(R.id.paletteTitle)).setText(p.getTitle());
        ((TextView) view.findViewById(R.id.paletteId)).setText("#" + (position + 1));

        LinearLayout colorsLayout = (LinearLayout) view.findViewById(R.id.colorsLayout);

        colorsLayout.removeAllViews();

        view.setId(position);

        for (String color : p.getColors()) {
            TextView newTv = new TextView(ctx);
            newTv.setBackgroundColor(Color.parseColor("#" + color));

            newTv.setLayoutParams(new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.MATCH_PARENT, 1.0f));

            colorsLayout.addView(newTv);
        }



        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, SelectPaletteWithMenuActivity.class);
                intent.putExtra("id", v.getId());
                MemoryDB.setLocalPalette(MemoryDB.getPalettes().get(v.getId()));
                ctx.startActivity(intent);
            }
        });

        return view;
    }

    Palette getPalette(int position) {
        return ((Palette) getItem(position));
    }

}
