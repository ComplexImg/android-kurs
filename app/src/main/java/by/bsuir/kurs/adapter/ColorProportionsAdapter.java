package by.bsuir.kurs.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import java.util.List;

import by.bsuir.kurs.R;
import by.bsuir.kurs.appData.MemoryDB;
import by.bsuir.kurs.model.Proportion;

public class ColorProportionsAdapter extends BaseAdapter {

    Context ctx;
    LayoutInflater lInflater;

    List<Proportion> proportionsList;
    int paletteObjId;

    public ColorProportionsAdapter(Context context, List<Proportion> proportions, int paletteObjId) {
        ctx = context;
        this.proportionsList = proportions;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return proportionsList.size();
    }

    @Override
    public Object getItem(int position) {
        return proportionsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.prop_item, parent, false);
        }

        Proportion p = getProp(position);

        view.setId(position);

        TextView color = (TextView) view.findViewById(R.id.colorTv);
        color.setText("#" + p.getColor());
        color.setBackgroundColor(Color.parseColor("#" + p.getColor()));

        Button deleteColor = (Button) view.findViewById(R.id.deleteColorBtn);
        deleteColor.setText("" + p.getProportion());



        return view;
    }

    Proportion getProp(int position) {
        return ((Proportion) getItem(position));
    }

}
