package by.bsuir.kurs.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import java.util.List;

import by.bsuir.kurs.R;
import by.bsuir.kurs.appData.MemoryDB;

public class ColorsAdapter extends BaseAdapter {

    Context ctx;
    LayoutInflater lInflater;
    List<String> colors;
    int paletteObjId;

    public ColorsAdapter(Context context, List<String> colors, int paletteObjId) {
        ctx = context;
        this.colors = colors;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return colors.size();
    }

    @Override
    public Object getItem(int position) {
        return colors.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.color_item, parent, false);
        }

        String p = getColor(position);
        view.setId(position);

        TextView color = (TextView) view.findViewById(R.id.colorTv);
        color.setText("#" + p);
        color.setBackgroundColor(Color.parseColor("#" + p));

        Button deleteColor = (Button) view.findViewById(R.id.deleteColorBtn);

        deleteColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ( MemoryDB.getLocalPalette().getColors().get(position) != null){
                    MemoryDB.getLocalPalette().getColors().remove(position);
                    ColorsAdapter.this.notifyDataSetChanged();
                }


            }
        });


        return view;
    }

    String getColor(int position) {
        return ((String) getItem(position));
    }

}
