package by.bsuir.kurs.adapter;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import by.bsuir.kurs.R;
import by.bsuir.kurs.SelectResultColorWithMenuActivity;
import by.bsuir.kurs.appData.MemoryDB;
import by.bsuir.kurs.model.Palette;
import by.bsuir.kurs.model.ResultColor;

public class ResultColorsAdapter extends BaseAdapter {

    Context ctx;
    LayoutInflater lInflater;
    List<ResultColor> resultColors;

    public ResultColorsAdapter(Context context, List<ResultColor> colors) {
        ctx = context;
        this.resultColors = colors;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return resultColors.size();
    }

    @Override
    public Object getItem(int position) {
        return resultColors.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.palette_item, parent, false);
        }

        ResultColor p = getColor(position);


        ((TextView) view.findViewById(R.id.paletteTitle)).setText(p.getTitle());
        ((TextView) view.findViewById(R.id.paletteId)).setText("#" + (position + 1));

        LinearLayout colorsLayout = (LinearLayout) view.findViewById(R.id.colorsLayout);

        colorsLayout.removeAllViews();

        view.setId(position);


        TextView newTv = new TextView(ctx);
        newTv.setBackgroundColor(Color.parseColor("#" + p.getColor()));
        newTv.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.MATCH_PARENT, 1.0f));
        colorsLayout.addView(newTv);


        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, SelectResultColorWithMenuActivity.class);

                int id = v.getId();

                ResultColor res = MemoryDB.getColors().get(id);

                MemoryDB.setLocalResultColor(MemoryDB.getColors().get(v.getId()));

                intent.putExtra("id", id);
                ctx.startActivity(intent);
            }
        });

        return view;
    }

    ResultColor getColor(int position) {
        return ((ResultColor) getItem(position));
    }

}
