package by.bsuir.kurs;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import by.bsuir.kurs.adapter.ColorsAdapter;
import by.bsuir.kurs.appData.MemoryDB;
import by.bsuir.kurs.appData.SharedDB;
import by.bsuir.kurs.model.Palette;
import by.bsuir.kurs.retrofit.Rest;
import okhttp3.Credentials;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectPaletteWithMenuActivity extends BaseActivity {


    TextView tvId;
    EditText titleText;
    Button saveBtn, deleteBtn, addColorBtn;
    ColorsAdapter colorsAdapter;

    SharedDB db;
    Palette palette;
    int id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_palette_with_menu);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Intent intent = getIntent();
        id = intent.getIntExtra("id", -1);

        tvId = (TextView) findViewById(R.id.tvId);
        titleText = (EditText) findViewById(R.id.titleText);
        saveBtn = (Button) findViewById(R.id.saveBtn);
        deleteBtn = (Button) findViewById(R.id.deleteBtn);
        addColorBtn = (Button) findViewById(R.id.addColorBtn);

        palette = MemoryDB.getLocalPalette();

        db = SharedDB.getObj(SelectPaletteWithMenuActivity.this);

        if (palette.getId() == null && id == -1) {
            setTitle("Новая палитра");

            deleteBtn.setEnabled(false);
        } else {
            setTitle("Палитра");
            deleteBtn.setEnabled(true);
        }

        if (palette != null && palette.getColors() != null) {

            palette = MemoryDB.getLocalPalette();

            tvId.setText("#" + (palette.getId() != null ? palette.getId() : "New"));
            titleText.setText(palette.getTitle());

            ListView lv = (ListView) findViewById(R.id.selectPaletteColors);
            colorsAdapter = new ColorsAdapter(this, palette.getColors(), id);
            lv.setAdapter(colorsAdapter);

        }


        addColorBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SelectPaletteWithMenuActivity.this, ColorPickerActivity.class);
                startActivity(intent);
            }
        });


        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                palette.setTitle(titleText.getText().toString());

                if ((palette.getId() != null && id != -1) || (palette.getId() == null && id != -1)) {
                    if (MemoryDB.isNetwork()) {
                        Rest.getPaletteApi().update(Credentials.basic(MemoryDB.getUser().getUsername(), MemoryDB.getUser().getPassword()), palette).enqueue(new Callback<Palette>() {
                            @Override
                            public void onResponse(Call<Palette> call, Response<Palette> response) {
                                Palette answerPalette = response.body();
                                MemoryDB.getPalettes().remove(id);
                                MemoryDB.getPalettes().add(id, palette);
                                db.updatePalettes(MemoryDB.getPalettes());
                                finish();
                            }

                            @Override
                            public void onFailure(Call<Palette> call, Throwable t) {
                                Toast.makeText(SelectPaletteWithMenuActivity.this, "Не получилось сходить на сервер!", Toast.LENGTH_SHORT).show();
                                MemoryDB.setNetwork(false);
                                db.setPalettesSyncFlag(false);
                                MemoryDB.getPalettes().remove(id);
                                MemoryDB.getPalettes().add(id, palette);
                                db.updatePalettes(MemoryDB.getPalettes());
                                finish();
                            }
                        });
                    }else{
                        db.setPalettesSyncFlag(false);
                        MemoryDB.getPalettes().remove(id);
                        MemoryDB.getPalettes().add(id, palette);
                        db.updatePalettes(MemoryDB.getPalettes());
                        finish();
                    }
                } else {
                    if (MemoryDB.isNetwork()) {
                        Rest.getPaletteApi().add(Credentials.basic(MemoryDB.getUser().getUsername(), MemoryDB.getUser().getPassword()), palette).enqueue(new Callback<Palette>() {
                            @Override
                            public void onResponse(Call<Palette> call, Response<Palette> response) {
                                Palette answerPalette = response.body();
                                MemoryDB.getPalettes().add(answerPalette);
                                db.updatePalettes(MemoryDB.getPalettes());
                                finish();
                            }

                            @Override
                            public void onFailure(Call<Palette> call, Throwable t) {
                                Toast.makeText(SelectPaletteWithMenuActivity.this, "Не получилось сходить на сервер!", Toast.LENGTH_SHORT).show();
                                MemoryDB.setNetwork(false);
                                db.setPalettesSyncFlag(false);
                                finish();
                            }
                        });
                    } else {
                        MemoryDB.getPalettes().add(palette);
                        db.updatePalettes(MemoryDB.getPalettes());
                        db.setPalettesSyncFlag(false);
                        finish();
                    }

                }

            }
        });


        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder ad;
                ad = new AlertDialog.Builder(SelectPaletteWithMenuActivity.this);
                ad.setTitle("Вы уверены что хотите...");
                ad.setMessage("УДАЛИТЬ ЭТУ ПАЛИТРУ?!");
                ad.setPositiveButton("Отмена", null);
                ad.setNegativeButton("Да", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {

                        if (MemoryDB.isNetwork()) {
                            Rest.getPaletteApi().delete(Credentials.basic(MemoryDB.getUser().getUsername(), MemoryDB.getUser().getPassword()), palette.getId()).enqueue(new Callback<Boolean>() {
                                @Override
                                public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                                    MemoryDB.getPalettes().remove(id);
                                    db.updatePalettes(MemoryDB.getPalettes());
                                    finish();
                                }

                                @Override
                                public void onFailure(Call<Boolean> call, Throwable t) {
                                    Toast.makeText(SelectPaletteWithMenuActivity.this, "Не получилось сходить на сервер!", Toast.LENGTH_SHORT).show();
                                    MemoryDB.getPalettes().remove(id);
                                    MemoryDB.setNetwork(false);
                                    db.updatePalettes(MemoryDB.getPalettes());
                                    db.setPalettesSyncFlag(false);
                                    finish();
                                }
                            });
                        } else {
                            MemoryDB.getPalettes().remove(id);
                            db.updatePalettes(MemoryDB.getPalettes());
                            db.setPalettesSyncFlag(false);
                            finish();
                        }

                    }
                });

                ad.show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        int color = MemoryDB.getReturnColor();

        if (color != 0) {
            String cl = Integer.toHexString(color);
            cl = cl.substring(2, cl.length());
            if (MemoryDB.getLocalPalette().getColors().indexOf(cl) == -1) {
                MemoryDB.getLocalPalette().getColors().add(cl);
                colorsAdapter.notifyDataSetChanged();
            }
            MemoryDB.setReturnColor(0);
        }
    }
}
