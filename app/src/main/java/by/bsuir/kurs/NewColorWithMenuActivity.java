package by.bsuir.kurs;

import android.content.Intent;
import android.content.Loader;
import android.graphics.Color;
import android.provider.Telephony;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import android.app.LoaderManager.LoaderCallbacks;


import by.bsuir.kurs.appData.MemoryDB;
import by.bsuir.kurs.genAlg.ResultColorAlgLoader;
import by.bsuir.kurs.model.Palette;
import by.bsuir.kurs.model.ResultColor;

public class NewColorWithMenuActivity extends BaseActivity implements LoaderCallbacks<ResultColor> {

    Spinner spinner;

    Button addColorBtn, goWork;

    String color;
    int paletteId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_color_with_menu);

        setTitle("Новый цвет");

        if (MemoryDB.getPalettes() == null ){
            finish();
        }
        if(MemoryDB.getPalettes().size() == 0){
            finish();
        }



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        getLoaderManager().initLoader(0, null, this);

        addColorBtn = (Button) findViewById(R.id.addColorBtn);
        goWork = (Button) findViewById(R.id.goWork);
        goWork.setEnabled(false);

        spinner = (Spinner) findViewById(R.id.spinner);




        String[] data = new String[MemoryDB.getPalettes().size()];
        int i = 0;
        for (Palette pal : MemoryDB.getPalettes()) {
            data[i] = new String(pal.getTitle());
            ++i;
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_palettes_item, data);
        adapter.setDropDownViewResource(R.layout.spinner_palettes_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setSelection(0);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                paletteId = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        goWork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cl = addColorBtn.getText().toString();
                color = cl.substring(1);

                Loader<ResultColor> loader;
                Bundle bndl = new Bundle();
                bndl.putString("color", color);
                bndl.putInt("paletteId", paletteId);
                loader = getLoaderManager().restartLoader(0, bndl, NewColorWithMenuActivity.this);
                goWork.setEnabled(false);
                loader.forceLoad();
            }
        });


        addColorBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewColorWithMenuActivity.this, ColorPickerActivity.class);
                startActivity(intent);
            }
        });

    }


    @Override
    public Loader<ResultColor> onCreateLoader(int id, Bundle args) {
        Loader<ResultColor> loader = null;
        if (id == 0) {
            Bundle bndl = new Bundle();
            bndl.putString("color", color);
            bndl.putInt("paletteId", paletteId);
            loader = new ResultColorAlgLoader(this, bndl);
        }
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<ResultColor> loader, ResultColor result) {
        goWork.setEnabled(true);
        Intent intent = new Intent(NewColorWithMenuActivity.this, SelectResultColorWithMenuActivity.class);
        MemoryDB.setLocalResultColor(result);
        startActivity(intent);
    }

    @Override
    public void onLoaderReset(Loader<ResultColor> loader) {

    }


    @Override
    protected void onResume() {
        super.onResume();
        int color = MemoryDB.getReturnColor();

        if (color != 0) {
            String cl = Integer.toHexString(color);
            cl = cl.substring(2, cl.length());

            addColorBtn.setBackgroundColor(Color.parseColor("#" + cl));
            addColorBtn.setText("#" + cl);
            goWork.setEnabled(true);

            MemoryDB.setReturnColor(0);
        }
    }
}
