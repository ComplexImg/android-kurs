package by.bsuir.kurs;

import android.content.Intent;
import android.os.Bundle;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import by.bsuir.kurs.adapter.PalettesAdapter;
import by.bsuir.kurs.appData.MemoryDB;
import by.bsuir.kurs.model.Palette;

public class PalettesListWithMenuActivity extends BaseActivity{

    PalettesAdapter palettesAdapter;
    ListView lv;

    Button tempAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_palettes_list_with_menu);

        setTitle("Мои палитры");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);



        lv = (ListView) findViewById(R.id.palettes);
        palettesAdapter = new PalettesAdapter(this, MemoryDB.getPalettes());
        lv.setAdapter(palettesAdapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PalettesListWithMenuActivity.this, SelectPaletteWithMenuActivity.class);
                Palette palette = new Palette();
                MemoryDB.setLocalPalette(palette);
                startActivity(intent);
            }
        });


    }

    @Override
    protected void onRestart() {
        super.onRestart();
        palettesAdapter.notifyDataSetChanged();
    }
}