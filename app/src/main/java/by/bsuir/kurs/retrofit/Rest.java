package by.bsuir.kurs.retrofit;

import android.app.Application;

import java.util.concurrent.TimeUnit;

import by.bsuir.kurs.appData.MemoryDB;
import by.bsuir.kurs.retrofit.api.PaletteApi;
import by.bsuir.kurs.retrofit.api.RegisterApi;
import by.bsuir.kurs.retrofit.api.ResultColorApi;
import by.bsuir.kurs.retrofit.api.SyncApi;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Rest  extends Application{

    private static PaletteApi paletteApi;
    private static RegisterApi registerApi;
    private static ResultColorApi resultColorApi;
    private static SyncApi syncApi;
    private Retrofit retrofit;

    @Override
    public void onCreate() {
        super.onCreate();

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(5, TimeUnit.SECONDS)
                .writeTimeout(5, TimeUnit.SECONDS)
                .readTimeout(5, TimeUnit.SECONDS)
                .build();



        retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.1.2:8080/")
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        syncApi = retrofit.create(SyncApi.class);
        paletteApi = retrofit.create(PaletteApi.class);
        registerApi = retrofit.create(RegisterApi.class);
        resultColorApi = retrofit.create(ResultColorApi.class);
    }

    public static SyncApi getSyncApi() {
        return syncApi;
    }

    public static PaletteApi getPaletteApi() {
        return paletteApi;
    }

    public static RegisterApi getRegisterApi() {
        return registerApi;
    }

    public static ResultColorApi getResultColorApi() {
        return resultColorApi;
    }
}
