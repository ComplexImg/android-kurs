package by.bsuir.kurs.retrofit.api;


import java.util.List;

import by.bsuir.kurs.model.Palette;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface PaletteApi {

    @GET("/color/palettes/read")
    Call<List<Palette>> read(@Header("Authorization") String credentials);

    @POST("/color/palettes/update")
    Call<Palette> update(@Header("Authorization") String credentials, @Body Palette palette);

    @POST("/color/palettes/add")
    Call<Palette> add(@Header("Authorization") String credentials, @Body Palette palette);

    @DELETE("/color/palettes/{id}/delete")
    Call<Boolean> delete(@Header("Authorization") String credentials, @Path("id") int paletteId);
}
