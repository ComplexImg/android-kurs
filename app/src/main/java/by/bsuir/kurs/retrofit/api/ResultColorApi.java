package by.bsuir.kurs.retrofit.api;

import java.util.List;

import by.bsuir.kurs.model.ResultColor;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ResultColorApi {
    @GET("/color/colors/read")
    Call<List<ResultColor>> read(@Header("Authorization") String credentials);

    @POST("/color/colors/update")
    Call<ResultColor> update(@Header("Authorization") String credentials, @Body ResultColor resultColor);

    @POST("/color/colors/add")
    Call<ResultColor> add(@Header("Authorization") String credentials, @Body ResultColor resultColor);

    @DELETE("/color/colors/{id}/delete")
    Call<Boolean> delete(@Header("Authorization") String credentials, @Path("id") int colorId);

}
