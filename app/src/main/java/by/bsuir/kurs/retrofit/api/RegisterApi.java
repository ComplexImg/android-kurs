package by.bsuir.kurs.retrofit.api;

import by.bsuir.kurs.model.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface RegisterApi {

    @POST("/color/signup/")
    Call<User> register(@Body User user);
    

}
