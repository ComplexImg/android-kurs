package by.bsuir.kurs.retrofit.api;


import java.util.List;

import by.bsuir.kurs.model.Palette;
import by.bsuir.kurs.model.ResultColor;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;


public interface SyncApi {

    @POST("/color/sync/palettes")
    Call<Boolean> syncPalettes(@Header("Authorization") String credentials, @Body List<Palette> palettes);

    @POST("/color/sync/colors")
    Call<Boolean> syncColors(@Header("Authorization") String credentials, @Body List<ResultColor> colors);

}
