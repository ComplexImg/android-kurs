package by.bsuir.kurs.other;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {

    public static boolean checkEngString(String string){
        Pattern p = Pattern.compile("^[a-zA-Z0-9_]{3,32}$");
        Matcher m = p.matcher(string);
        return m.matches();
    }

    public static boolean checkString(String string){
        Pattern p = Pattern.compile("^[a-z0-9_-а-яА-Я]{3,32}$");
        Matcher m = p.matcher(string);
        return m.matches();
    }

    public static boolean checkEmail(String string){
        Pattern p = Pattern.compile("^[a-zA-Z0-9]{3,}[@][a-zA-Z0-9]{2,}[.][a-zA-Z]{2,}$");
        Matcher m = p.matcher(string);
        return m.matches();
    }


}
