package by.bsuir.kurs.loader;

import android.content.Context;

import java.io.IOException;
import java.util.List;

import by.bsuir.kurs.appData.MemoryDB;
import by.bsuir.kurs.appData.SharedDB;
import by.bsuir.kurs.loader.response.RequestResult;
import by.bsuir.kurs.loader.response.Response;
import by.bsuir.kurs.loader.response.ResultColorsResponse;
import by.bsuir.kurs.model.ResultColor;
import by.bsuir.kurs.model.User;
import by.bsuir.kurs.retrofit.Rest;
import by.bsuir.kurs.retrofit.api.ResultColorApi;
import okhttp3.Credentials;
import retrofit2.Call;

public class ResultColorsLoader extends BaseLoader {

    public ResultColorsLoader(Context context) {
        super(context);
    }

    @Override
    protected Response apiCall() throws IOException {
        ResultColorApi service = Rest.getResultColorApi();

        User user = MemoryDB.getUser();

        Call<List<ResultColor>> call = service.read(Credentials.basic(user.getUsername(), user.getPassword()));
        List<ResultColor> resultColors = call.execute().body();

        if (resultColors == null) {
            return new ResultColorsResponse()
                    .setRequestResult(RequestResult.BAD_USER);
        } else {
            return new ResultColorsResponse()
                    .setRequestResult(RequestResult.SUCCESS)
                    .setAnswer(resultColors);
        }
    }

    @Override
    protected Response getLocalData(Context context) throws IOException {

        SharedDB db = SharedDB.getObj(context);
        List<ResultColor> resultColors = db.getResultColors();

        MemoryDB.setColors(resultColors);

        return new ResultColorsResponse()
                .setRequestResult(RequestResult.SUCCESS)
                .setAnswer(resultColors);

    }


}
