package by.bsuir.kurs.loader;

import android.content.Context;

import java.io.EOFException;
import java.io.IOException;
import java.util.List;

import by.bsuir.kurs.appData.MemoryDB;
import by.bsuir.kurs.appData.SharedDB;
import by.bsuir.kurs.loader.response.PalettesResponse;
import by.bsuir.kurs.loader.response.RequestResult;
import by.bsuir.kurs.loader.response.Response;
import by.bsuir.kurs.loader.response.UserResponse;
import by.bsuir.kurs.model.Palette;
import by.bsuir.kurs.model.User;
import by.bsuir.kurs.retrofit.Rest;
import by.bsuir.kurs.retrofit.api.PaletteApi;
import by.bsuir.kurs.retrofit.api.RegisterApi;
import okhttp3.Credentials;
import retrofit2.Call;

public class UserLoader extends BaseLoader {

    public UserLoader(Context context) {
        super(context);
    }

    @Override
    protected Response apiCall() throws IOException {
        RegisterApi service = Rest.getRegisterApi();

        User user = MemoryDB.getUser();

        Call<User> call = service.register(user);

        try{
            User newUser = call.execute().body();

            if (newUser == null) {
                return new UserResponse()
                        .setRequestResult(RequestResult.BAD_USER);
            } else {
                return new UserResponse()
                        .setRequestResult(RequestResult.SUCCESS)
                        .setAnswer(newUser);
            }

        }catch (EOFException e){
            // неверный юзер

            return new UserResponse()
                    .setRequestResult(RequestResult.BAD_USER);

        }




    }

    @Override
    protected Response getLocalData(Context context) throws IOException {

        return new UserResponse()
                .setRequestResult(RequestResult.ERROR)
                .setAnswer(null);

    }


}
