package by.bsuir.kurs.loader.response;

public enum RequestResult {

    SUCCESS,
    ERROR,
    BAD_USER

}
