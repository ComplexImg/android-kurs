package by.bsuir.kurs.loader;


import android.content.AsyncTaskLoader;
import android.content.Context;

import java.io.IOException;

import by.bsuir.kurs.appData.MemoryDB;
import by.bsuir.kurs.loader.response.RequestResult;
import by.bsuir.kurs.loader.response.Response;


public abstract class BaseLoader extends AsyncTaskLoader<Response> {


    public BaseLoader(Context context) {
        super(context);
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    @Override
    public Response loadInBackground() {
        try {
            Response response = apiCall();
            if (response.getRequestResult() == RequestResult.SUCCESS) {
                response.save(getContext());
                onSuccess();
            } else if (response.getRequestResult() == RequestResult.BAD_USER){
                return response;
            }else{
                onError();
            }
            return response;
        } catch (IOException e) {
            try {
                // интернета нет или проблемы с ответом - читаем из локальной бд
                onError();
                return getLocalData(getContext());

            } catch (Exception ex) {
                // возникла ошибка при чтении из БД (она пуста)
                return new Response();
            }
        }
    }

    protected void onSuccess() {
    }

    protected void onError() {
        // интернета нет или проблемы с ответом - ставим флаг, чтобы в след раз сразу читать из БД
        MemoryDB.setNetwork(false);
    }

    protected abstract Response apiCall() throws IOException;

    protected abstract Response getLocalData(Context context) throws IOException;
}
