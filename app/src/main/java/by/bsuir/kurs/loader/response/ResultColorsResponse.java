package by.bsuir.kurs.loader.response;


import android.content.Context;

import java.util.List;

import by.bsuir.kurs.appData.MemoryDB;
import by.bsuir.kurs.appData.SharedDB;
import by.bsuir.kurs.model.ResultColor;


public class ResultColorsResponse extends Response {

    @Override
    public void save(Context context) {
        List<ResultColor> resultColorList = getTypedAnswer();
        if (resultColorList != null && resultColorList.size() != 0) {

            //save in db
            MemoryDB.setColors(resultColorList);

            SharedDB db = SharedDB.getObj(context);
            db.updateResultColors(resultColorList);
        }
    }
}
