package by.bsuir.kurs.loader.response;


import android.content.Context;

import by.bsuir.kurs.appData.MemoryDB;
import by.bsuir.kurs.appData.SharedDB;
import by.bsuir.kurs.model.User;


public class UserResponse extends Response {

    @Override
    public void save(Context context) {
        User user = getTypedAnswer();
        if (user != null && user.getUsername() != null) {

            //save in db
            MemoryDB.setUser(user);

            SharedDB db = SharedDB.getObj(context);
            db.updateUser(user);
        }
    }
}
