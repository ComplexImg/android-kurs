package by.bsuir.kurs.loader;

import android.content.Context;

import java.io.IOException;
import java.util.List;

import by.bsuir.kurs.appData.MemoryDB;
import by.bsuir.kurs.appData.SharedDB;
import by.bsuir.kurs.loader.response.PalettesResponse;
import by.bsuir.kurs.loader.response.RequestResult;
import by.bsuir.kurs.loader.response.Response;
import by.bsuir.kurs.model.Palette;
import by.bsuir.kurs.model.User;
import by.bsuir.kurs.retrofit.Rest;
import by.bsuir.kurs.retrofit.api.PaletteApi;
import okhttp3.Credentials;
import retrofit2.Call;

public class PalettesLoader extends BaseLoader {

    public PalettesLoader(Context context) {
        super(context);
    }

    @Override
    protected Response apiCall() throws IOException {
        PaletteApi service = Rest.getPaletteApi();

        User user = MemoryDB.getUser();

        Call<List<Palette>> call = service.read(Credentials.basic(user.getUsername(), user.getPassword()));
        List<Palette> palettes = call.execute().body();

        if (palettes == null) {
            return new PalettesResponse()
                    .setRequestResult(RequestResult.BAD_USER);
        } else {
            return new PalettesResponse()
                    .setRequestResult(RequestResult.SUCCESS)
                    .setAnswer(palettes);
        }
    }

    @Override
    protected Response getLocalData(Context context) throws IOException {

        SharedDB db = SharedDB.getObj(context);
        List<Palette> palettes = db.getPalettes();
        MemoryDB.setPalettes(palettes);

        return new PalettesResponse()
                .setRequestResult(RequestResult.SUCCESS)
                .setAnswer(palettes);

    }


}
