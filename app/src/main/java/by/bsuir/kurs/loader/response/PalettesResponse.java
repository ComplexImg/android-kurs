package by.bsuir.kurs.loader.response;


import android.content.Context;

import java.util.List;

import by.bsuir.kurs.appData.MemoryDB;
import by.bsuir.kurs.appData.SharedDB;
import by.bsuir.kurs.model.Palette;
import by.bsuir.kurs.model.User;


public class PalettesResponse extends Response {

    @Override
    public void save(Context context) {
        List<Palette> paletteList = getTypedAnswer();
        if (paletteList != null) {

            //save in db
            MemoryDB.setPalettes(paletteList);

            SharedDB db = SharedDB.getObj(context);
            db.updatePalettes(paletteList);
        }
    }
}
