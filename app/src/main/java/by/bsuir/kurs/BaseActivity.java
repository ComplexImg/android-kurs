package by.bsuir.kurs;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import by.bsuir.kurs.appData.MemoryDB;


public class BaseActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.reg) {
            Intent intent = new Intent(BaseActivity.this, RegisterWithMenuActivity.class);
            startActivity(intent);
        } else if (id == R.id.my_palettes) {
            if(MemoryDB.getUser() != null && MemoryDB.getPalettes() != null && MemoryDB.getPalettes().size() >= 0){
                Intent intent = new Intent(BaseActivity.this, PalettesListWithMenuActivity.class);
                startActivity(intent);
            }
        } else if (id == R.id.res_colors) {


            if(MemoryDB.getUser() != null && MemoryDB.getPalettes() != null && MemoryDB.getPalettes().size() > 0 ){
                Intent intent = new Intent(BaseActivity.this, ResultColorsListWithMenu.class);
                startActivity(intent);
            }
        } else if (id == R.id.new_color) {
            if(MemoryDB.getUser() != null && MemoryDB.getPalettes() != null && MemoryDB.getPalettes().size() >= 0){
                Intent intent = new Intent(BaseActivity.this, NewColorWithMenuActivity.class);
                startActivity(intent);
            }
        } else if (id == R.id.login) {
            Intent intent = new Intent(BaseActivity.this, MainActivity.class);
            startActivity(intent);
        } else if (id == R.id.netw) {
            Intent intent = new Intent(BaseActivity.this, SettingActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
