package by.bsuir.kurs.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Proportion {

    @SerializedName("proportion")
    @Expose
    private int proportion;
    @SerializedName("color")
    @Expose
    private String color;

    public Proportion(){}

    public Proportion(String color, int proportion){
        this.color = color;
        this.proportion = proportion;
    }

    public int getProportion() {
        return proportion;
    }

    public void setProportion(int proportion) {
        this.proportion = proportion;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Proportion{" +
                "proportion=" + proportion +
                ", color='" + color + '\'' +
                '}';
    }
}
