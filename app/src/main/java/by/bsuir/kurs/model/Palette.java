package by.bsuir.kurs.model;

import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Palette {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("date")
    @Expose
    private Long date;
    @SerializedName("colors")
    @Expose
    private List<String> colors = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public List<String> getColors() {
        return colors;
    }

    public void setColors(List<String> colors) {
        this.colors = colors;
    }

    public Palette(){
        id = null;
        title = "";
        colors = new ArrayList<String >();
        date = null;
    }



    @Override
    public String toString() {
        return "Palette{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", date=" + date +
                ", colors=" + colors +
                '}';
    }
}
