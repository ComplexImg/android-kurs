package by.bsuir.kurs.appData;

import java.util.ArrayList;
import java.util.List;

import by.bsuir.kurs.model.Palette;
import by.bsuir.kurs.model.ResultColor;
import by.bsuir.kurs.model.User;


public class MemoryDB {

    private static boolean network = true;

    private static String serverAddress = "http://192.168.1.2:8080/";

    private static User user;
    private static List<Palette> palettes;
    private static List<ResultColor> colors;

    private static List<String> localColorsArray;

    private static Palette localPalette;
    private static ResultColor localResultColor ;

    private static int returnColor;

    private static Boolean palettesIsSync = null;
    private static Boolean colorsIsSync = null;

    private MemoryDB(){}

    public static User getUser() {
        return user;
    }

    public static void setUser(User user) {
        MemoryDB.user = user;
    }

    public static List<Palette> getPalettes() {
        return palettes;
    }

    public static void setPalettes(List<Palette> palettes) {
        MemoryDB.palettes = palettes;
    }

    public static List<ResultColor> getColors() {
        return colors;
    }

    public static void setColors(List<ResultColor> colors) {
        MemoryDB.colors = colors;
    }

    public static boolean isNewUser()
    {
        return (user == null);
    }

    public static boolean isNetwork() {
        return network;
    }

    public static void setNetwork(boolean network) {
        MemoryDB.network = network;
    }

    public static boolean allFieldIsNotNull(){
        return (user != null && palettes != null && colors != null);
    }

    public static List<String> getLocalColorsArray() {
        return localColorsArray;
    }

    public static void setLocalColorsArray(List<String> localColorsArray) {
        MemoryDB.localColorsArray = new ArrayList<String >(localColorsArray);
    }

    public static Palette getLocalPalette() {
        return localPalette;
    }

    public static void setLocalPalette(Palette palette) {
        MemoryDB.localPalette = new Palette();
        localPalette.setId( palette.getId());
        localPalette.setTitle(palette.getTitle());
        localPalette.setDate(palette.getDate());
        localPalette.setColors(new ArrayList<String>(palette.getColors()));

    }

    public static String getServerAddress() {
        return serverAddress;
    }

    public static void setServerAddress(String serverAddress) {
        MemoryDB.serverAddress = serverAddress;
    }

    public static int getReturnColor() {
        return returnColor;
    }

    public static ResultColor getLocalResultColor() {
        return localResultColor;
    }

    public static void setLocalResultColor(ResultColor localResultColor) {
        MemoryDB.localResultColor = localResultColor;
    }

    public static void setReturnColor(int returnColor) {
        MemoryDB.returnColor = returnColor;
    }

    public static boolean palettesIsSync() {
        return palettesIsSync;
    }

    public static void setPalettesIsSync(boolean palettesIsSync) {
        MemoryDB.palettesIsSync = palettesIsSync;
    }

    public static boolean colorsIsSync() {
        return colorsIsSync;
    }

    public static void setColorsIsSync(boolean colorsIsSync) {
        MemoryDB.colorsIsSync = colorsIsSync;
    }
}
