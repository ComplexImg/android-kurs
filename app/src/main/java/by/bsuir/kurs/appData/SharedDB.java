package by.bsuir.kurs.appData;


import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;
import java.util.Objects;


import by.bsuir.kurs.model.Palette;
import by.bsuir.kurs.model.ResultColor;
import by.bsuir.kurs.model.User;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;

public class SharedDB {

    private static final String USER_FIELD = "user_field";
    private static final String PALETTES_FIELD = "palettes_field";
    private static final String RESULT_COLORS_FIELD = "result_colors_field";
    private static final String RESULT_COLORS_SYNC_FLAG = "result_colors_sync_flag";
    private static final String PALETTES_SYNC_FLAG = "palettes_sync_flag";
    private static final String SERVER_ADDRESS = "server_address";

    private static SharedPreferences sharedPreferences;
    private static SharedDB obj;
    private static Gson gson;

    private static Boolean palettesSyncFlag = null;
    private static Boolean colorsSyncFlag = null;

    private Context context;

    private SharedDB(Context con) {
        this.context = con;
    }

    public static SharedDB getObj(Context con) {
        if (obj == null) {
            obj = new SharedDB(con);
            gson = new Gson();
        }
        return obj;
    }

    // user -------------------

    public void updateUser(User user) {
        sharedPreferences = getDefaultSharedPreferences(context);
        SharedPreferences.Editor ed = sharedPreferences.edit();
        String userString = gson.toJson(user);
        ed.putString(USER_FIELD, userString);
        ed.commit();
    }

    public User getUser() {
        sharedPreferences = getDefaultSharedPreferences(context);
        String userString = sharedPreferences.getString(USER_FIELD, "");
        if (userString.equals("")) {
            return null;
        }
        return gson.fromJson(userString, User.class);
    }


    // palette_item -------------------

    public void updatePalettes(List<Palette> palettes){
        sharedPreferences = getDefaultSharedPreferences(context);
        SharedPreferences.Editor ed = sharedPreferences.edit();
        String palettesString = gson.toJson(palettes);
        ed.putString(PALETTES_FIELD, palettesString);
        ed.commit();
    }

    public List<Palette> getPalettes(){
        sharedPreferences = getDefaultSharedPreferences(context);
        String palettesString = sharedPreferences.getString(PALETTES_FIELD, "[]");
        if (palettesString.equals("")) {
            return null;
        }
        return gson.fromJson(palettesString, new TypeToken<List<Palette>>(){}.getType());
    }


    // result_color -------------------

    public void updateResultColors(List<ResultColor> resultColors){
        sharedPreferences = getDefaultSharedPreferences(context);
        SharedPreferences.Editor ed = sharedPreferences.edit();
        String resultColorsString = gson.toJson(resultColors);
        ed.putString(RESULT_COLORS_FIELD, resultColorsString);
        ed.commit();
    }

    public List<ResultColor> getResultColors(){
        sharedPreferences = getDefaultSharedPreferences(context);
        String resultColorsString = sharedPreferences.getString(RESULT_COLORS_FIELD, "[]");
        if (resultColorsString.equals("")) {
            return null;
        }
        return gson.fromJson(resultColorsString, new TypeToken<List<ResultColor>>(){}.getType());
    }



    // server address -------------------

    public void updateServerAddress(String addr){
        sharedPreferences = getDefaultSharedPreferences(context);
        SharedPreferences.Editor ed = sharedPreferences.edit();
        ed.putString(SERVER_ADDRESS, addr);
        ed.commit();
    }

    public String getServerAddress(){
        sharedPreferences = getDefaultSharedPreferences(context);
        return sharedPreferences.getString(SERVER_ADDRESS, "http://192.168.1.2:8080/");
    }


    // flags -------------------

    public void setColorsSyncFlag(Boolean flag){
        if (colorsSyncFlag != flag){
            sharedPreferences = getDefaultSharedPreferences(context);
            SharedPreferences.Editor ed = sharedPreferences.edit();
            String syncFlag = (flag ? "true" : "false");
            ed.putString(RESULT_COLORS_SYNC_FLAG, syncFlag);
            ed.commit();

            colorsSyncFlag = flag;
        }
    }

    public boolean getColorsSyncFlag(){
        sharedPreferences = getDefaultSharedPreferences(context);
        String resultColorsFlag= sharedPreferences.getString(RESULT_COLORS_SYNC_FLAG, "true");
        return  resultColorsFlag.equals("true");
    }



    public void setPalettesSyncFlag(Boolean flag){
        if (palettesSyncFlag != flag){
            sharedPreferences = getDefaultSharedPreferences(context);
            SharedPreferences.Editor ed = sharedPreferences.edit();
            String syncFlag = (flag ? "true" : "false");
            ed.putString(PALETTES_SYNC_FLAG, syncFlag);
            ed.commit();
            palettesSyncFlag = flag;
        }

   }

    public boolean getPalettesSyncFlag(){
        sharedPreferences = getDefaultSharedPreferences(context);
        String resultColorsFlag= sharedPreferences.getString(PALETTES_SYNC_FLAG, "true");
        return  resultColorsFlag.equals("true");
    }


    public String tempGetPalettes(){
        sharedPreferences = getDefaultSharedPreferences(context);
        return  sharedPreferences.getString(PALETTES_FIELD, "");
    }

    public String tempGetColors(){
        sharedPreferences = getDefaultSharedPreferences(context);
        return  sharedPreferences.getString(RESULT_COLORS_FIELD, "");
    }

    public String tempGetUser(){
        sharedPreferences = getDefaultSharedPreferences(context);
        return  sharedPreferences.getString(USER_FIELD, "");
    }

    public void clear(){
        sharedPreferences = getDefaultSharedPreferences(context);
        SharedPreferences.Editor ed = sharedPreferences.edit();
        ed.remove(USER_FIELD);
        ed.remove(PALETTES_FIELD);
        ed.remove(RESULT_COLORS_FIELD);
        ed.remove(RESULT_COLORS_SYNC_FLAG);
        ed.remove(PALETTES_SYNC_FLAG);
        ed.commit();
    }
}
