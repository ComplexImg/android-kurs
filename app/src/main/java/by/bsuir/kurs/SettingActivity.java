package by.bsuir.kurs;

import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import by.bsuir.kurs.appData.MemoryDB;
import by.bsuir.kurs.appData.SharedDB;
import by.bsuir.kurs.retrofit.Rest;

public class SettingActivity extends BaseActivity {

    Button deleteData, serverAddrBtn;
    EditText serverAddr;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        setTitle("Настройки");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);



        serverAddr = (EditText) findViewById(R.id.serverAddress);
        deleteData = (Button) findViewById(R.id.deleteData);
        serverAddrBtn = (Button) findViewById(R.id.serverAddrBtn);

        String newAddr = MemoryDB.getServerAddress();

        serverAddr.setText(MemoryDB.getServerAddress());

        serverAddrBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String newAddr = serverAddr.getText().toString();

                MemoryDB.setServerAddress(newAddr);
                SharedDB db = SharedDB.getObj(SettingActivity.this);
                db.updateServerAddress(newAddr);



            }
        });


        deleteData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedDB db = SharedDB.getObj(SettingActivity.this);
                db.clear();
                finishAffinity();
            }
        });

    }
}
